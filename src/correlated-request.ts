// this will be removed
export class CorrelatedRequestDO {
    correlationId: number;
    onFulFilled: Function;
    onError: Function;
    requestTime: number;

    constructor(correlationId: number, onFulFilled: Function, onError: Function) {
        this.correlationId = correlationId;
        this.onFulFilled = onFulFilled;
        this.onError = onError;
        this.requestTime = new Date().getTime();
    }
}


