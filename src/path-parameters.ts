import {QueryParameter} from './query-parameter';

export class PathParameters {
    _includeChildren = true;
    _fields: string[] = [];
    _predicates: QueryParameter[] = [];

    constructor(includeChildren?: boolean, fields?: string[], predicates?: QueryParameter[]) {
        if (includeChildren !== undefined) {
            this._includeChildren = includeChildren;
        } else {
            this._includeChildren = true;
        }
        if (fields !== undefined) {
            this._fields = fields;
        }
        if (predicates !== undefined) {
            this._predicates = predicates;
        }
    }

    getParameters(): any {
        const obj: any = {
            includeChildren: this._includeChildren
        };
        if (this._fields !== undefined) {
            obj['fields'] = this._fields;
        }
        if (this._predicates !== undefined) {
            obj['predicates'] = this._predicates;

        }
        return obj;
    }
}

