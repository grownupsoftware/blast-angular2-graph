export class BlastHelpers {
    static isPresent(obj: any): boolean {
        return obj !== undefined && obj !== null;
    }

    static isString(obj: any): boolean {
        return typeof obj === 'string';
    }

    static isArray(obj: any): boolean {
        return Array.isArray(obj);
    }

    static isFunction(obj: any): boolean {
        return typeof obj === 'function';
    }
    static isJson(obj: any): boolean {
        try {
            JSON.parse(obj);
            return true;
        } catch (exception) {
            return false;
        }
    }
};

