export class QueryParameter {
    _field: string;
    _operand: string;
    _value: string;

    constructor(field: string, operand: string, value: string) {
        this._field = field;
        this._operand = operand;
        this._value = value;
    }
}


