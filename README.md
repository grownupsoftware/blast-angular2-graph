# Blast-Graph-Angular2
![Alt text](./resources/images/b-circle-trans-100.png)
**with**
![Alt text](./resources/images/angular.png)


    
# Overview
This is a websocket client for Angular2 projects for connection to a *Blast Server*. It is an extension of [blast-angular2](https://bitbucket.org/grownupsoftware/blast-angular2) and provides additional **commands** (*functions*) relevant to the [blast-graph-module](https://bitbucket.org/grownupsoftware/blast-graph-module). It is worth reading about [blast-graph-module](https://bitbucket.org/grownupsoftware/blast-graph-module) before proceeding. 

# Building npm
### NPM Commands   
| Command|Description |
|---|---|
| npm run lint | runs lint
| npm run compile | compiles tyepscript
| npm run minify | minifies the output javascript
| npm run bundle | includes any 3rd party libraries
| npm run bundle-minify | minifies the bundle
| npm run prepublish | runs all above commands
| npm run dev | lint,compile and npm link
| npm run build | runs everything

#### To publish to npm
npm publish

Note: Although the main objective for this project is a 'npm module' it also serves as the *build* for our native javascript library.

    
# Getting Started

```
    npm install blast-graph-angular2

to build ..
npm run packagr
npm publish dist

```

```

// connect
const blastService: BlastService = new BlastService('ws://127.0.0.1:8081/blast');

// create an initialized variable for the local copy
let myBooks:any[] = [];

// attach local copy to server's data model, collection 'books'. 
// When server data changes then local copy will automatically be updated.

blastService.attach("books",myBooks);

```


# Usage
The following commands (functions) are an extension to those already available in blast-angular2.

## Commands

Command summary:

| Command|Description |
|---|---|
| add | Add an entity to the server's data graph
| update | Update an entity in the server's data graph
| remove | Remove an entity from the server's data graph
| attach | Attach local variable to a *collection* in the server's data graph and automatically update when there are server side changes
| detach | Associated local copy will no longer receive updates from the server
| detachAll | All attachments are removed - no local copies will be mainatined
| getAttachments | Returns a list of current attachments with the server
| fetch | Get a snapshot of a collection or entity from the server's data graph
| fetchRoot | Get a snapshot of all the data in the server's data graph
| getSchema | Get the schema of the server's data graph


### Add
Add new entity to the server's data graph.

**Function**

```
add(collection: string, entity: any): Promise<any>
```
**Example**

```
blastService.add("books",{bookId:1,name:'Hello World'}).then((response) => {
       console.log('that worked!');
   }).catch((error) => {
       console.log('oops! that failed');
   });
```

### Update
Update an entity in the server's data graph.

**Function**

```
update(key: string, entity: any): Promise<any>
```
**Example**

```
blastService.update("books/id:1",{bookId:1,name:'New World'}).then((response) => {
       console.log('that worked!');
   }).catch((error) => {
       console.log('oops! that failed');
   });
```

### Remove
Remove an entity in the server's data graph.

**Function**

```
remove(key: string): Promise<any>
```
**Example**

```
blastService.remove("books/id:1").then((response) => {
       console.log('book removed');
   }).catch((error) => {
       console.log('oops! that failed');
   });
```
### Attach
Attach a client side variable to a server side collection. Whenever there is a change to the data on the server (including any of its children) then automatically updated my local client version.

For the *key* please read [blast-graph-module](https://bitbucket.org/grownupsoftware/blast-graph-module) for a better overview.

**Function**

```
    attach(key: string, data: any, parameters?: PathParameters, modificationWatcher?: ModificationWatcher): Promise<any> {

```
**Example**

```
let allBooks:any[] = [];
let oneBook:any = {};

blastService.attach('books',allBooks);

blastService.attach('books/id:1',oneBook);

```
**Additional Parameters**

*PathParameters*
Apply filtering to the 'attach' or 'fetch' command.

| Paramter|Description |
|---|---|
| includeChildren | Default is true, but when false any children collections are not returned.
| fields | An array of field names - the returned data will only include these fields, and for 'attach' the local copy will only be updated if any of these fields change.

*Modifcation Watcher*

A callback can be associated with the 'attach' - so, when there are changes to the local copy, the 'added','changed' or 'removed' methods of ModicationWatcher will be called.

### Detach
Remove an 'attachment' - the local copy will no longer be maintained.

**Function**

```
detach(attachmentId: string)
```
**Example**

```
blastService.detach(attachmentId)

```

### DetachAll
Remove all attachments - local copies will no longer be maintained.

**Function**

```
detachAll()
```
**Example**

```
detachAll()

```

### getAttachments

Returns a list of current attachments.

**Function**

```
getAttachments()
```
**Example**

```
getAttachments()

```

### Fetch

Fetch a collection or record from the server, this is a snapshot of the current data held by the server. The resultant value will not get updated if there is a change to the server data.

**Function**

```
fetch(key: string, parameters?: PathParameters) 
```
**Example**

```
let allBooks:any[] = blastService.fetch("books")
let oneBooks:any = blastService.fetch("books/id:1")

```

### Fetch Root
Fetches the entire data graph from the server. Great in development! but strongly advise that this is not used in production - the resultant amount of data could be *huge*!

**Function**

```
fetchRoot();

```
**Example**

```
let myData:any = blastService.fetchRoot();
```
MyData would look like the following:

```
 {
    "books": []
    "authors": []
 }
```

### getSchema

Fetches the schema of the server's data graph.

**Function**

```
getSchema()
```
**Example**

```
blastService.getSchema();

```
