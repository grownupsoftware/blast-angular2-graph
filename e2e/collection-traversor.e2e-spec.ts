import {CollectionTraversor} from '../src/collection-traversor';
import {PathDetails} from '../src/path-details';
import {TEST_DATA} from './test-data.e2e-spec';

describe('CollectionTraversor - findRecord', () => {
    let testData: any;


    beforeEach(() => {
        testData = TEST_DATA;
    });

    it('should find an event with market of 126', () => {
        const data = CollectionTraversor.findRecord('events/id:119', testData);
        if (Object.prototype.toString.call(data) === '[object Array]') {
            fail('should not be an array');
        }
        expect(data.markets[0].id).toEqual(126);
    });


});

describe('CollectionTraversor - findRecordInList', () => {
    let testData: any;

    beforeEach(() => {
        testData = TEST_DATA.events;
    });

    it('should find an event with market of 107', () => {
        const data = CollectionTraversor.findRecordInList('id', '100', testData);
        if (Object.prototype.toString.call(data) === '[object Array]') {
            fail('should not be an array');
        }
        expect(data.markets[0].id).toEqual(107);
    });

    it('should find an event with market of 126', () => {
        const data = CollectionTraversor.findRecordInList('id', '119', testData);
        if (Object.prototype.toString.call(data) === '[object Array]') {
            fail('should not be an array');
        }
        expect(data.markets[0].id).toEqual(126);
    });

});

describe('CollectionTraversor - findRecordIndexInList', () => {
    let testData: any;

    beforeEach(() => {
        testData = TEST_DATA.events;
    });

    it('should find index of 0 for event 100', () => {
        const index = CollectionTraversor.findRecordIndexInList('id', '100', testData);
        expect(index).toEqual(0);
    });

    it('should find index of 1 for event 119', () => {
        const index = CollectionTraversor.findRecordIndexInList('id', '119', testData);
        expect(index).toEqual(1);
    });

});


describe('CollectionTraversor - findARecord', () => {
    let testData: any;

    beforeEach(() => {
        testData = TEST_DATA;
    });

    it('should find market of 107', () => {
        const pathDetails: PathDetails[] = PathDetails.splitPath('events/id:100/markets/id:107');
        const index = 1;
        const data = CollectionTraversor.findARecord(index, pathDetails, testData);
        if (Object.prototype.toString.call(data) === '[object Array]') {
            fail('should not be an array');
        }
        expect(data.id).toEqual(107);

    });

    it('should find runner of 110', () => {
        const pathDetails: PathDetails[] = PathDetails.splitPath('events/id:100/markets/id:107/runners/id:110');

        // 1st find market data
        let index = 1;
        const data = CollectionTraversor.findARecord(index, pathDetails, testData);

        // now find runner data
        index = 2;
        const runnerData = CollectionTraversor.findARecord(index, pathDetails, data);
        if (Object.prototype.toString.call(runnerData) === '[object Array]') {
            fail('should not be an array');
        }
        expect(runnerData.id).toEqual(110);

    });

});

describe('CollectionTraversor - traverseUntilFinalList', () => {
    let testData: any;

    beforeEach(() => {
        testData = TEST_DATA.events;
    });

    it('should find runner in last collection with no id specified', () => {
        const data = CollectionTraversor.traverseUntilFinalList('events/id:100/markets/id:107/runners', testData);
        if (Object.prototype.toString.call(data) !== '[object Array]') {
            fail('should be an array');
        }

        expect(data.length).toEqual(5);

        // find runner in returned list
        const runnerData = CollectionTraversor.findRecordInList('id', '110', data);
        expect(runnerData.id).toEqual(110);

    });

    it('should find runner in last collection even when id is specified', () => {
        const data = CollectionTraversor.traverseUntilFinalList('events/id:100/markets/id:107/runners/id:110', testData);
        if (Object.prototype.toString.call(data) !== '[object Array]') {
            fail('should be an array');
        }

        expect(data.length).toEqual(5);

        // find runner in returned list
        const runnerData = CollectionTraversor.findRecordInList('id', '110', data);
        expect(runnerData.id).toEqual(110);

    });

    it('should return original list if no keyfield details', () => {
        const data = CollectionTraversor.traverseUntilFinalList('events', testData);
        if (Object.prototype.toString.call(data) !== '[object Array]') {
            fail('should be an array');
        }

        expect(data.length).toEqual(2);

        // find event in returned (original) list
        const runnerData = CollectionTraversor.findRecordInList('id', '100', data);
        expect(runnerData.id).toEqual(100);

    });

});

describe('CollectionTraversor - matches', () => {

    it('should match 1 === 1', () => {
        expect(CollectionTraversor.matches(1, 1)).toEqual(true);
    });

    it('should match "1" === 1', () => {
        expect(CollectionTraversor.matches('1', 1)).toEqual(true);
    });

    it('should match 1 === "1"', () => {
        expect(CollectionTraversor.matches(1, '1')).toEqual(true);
    });

    it('should match "1" === "1"', () => {
        expect(CollectionTraversor.matches('1', '1')).toEqual(true);
    });

    it('should NOT match "1" === "1a"', () => {
        expect(CollectionTraversor.matches('1', '1a')).toEqual(false);
    });

    it('should NOT match 1 === "1a"', () => {
        expect(CollectionTraversor.matches(1, '1a')).toEqual(false);
    });

    it('should NOT match "1a" === "1"', () => {
        expect(CollectionTraversor.matches('1a', '1')).toEqual(false);
    });

    it('should NOT match "1a" === 1', () => {
        expect(CollectionTraversor.matches('1a', 1)).toEqual(false);
    });

});

describe('CollectionTraversor - findList', () => {
    let testData: any;


    beforeEach(() => {
        testData = TEST_DATA.events;
    });

    it('should find markets for an event with a length of 3', () => {
        const data = CollectionTraversor.findList('events/id:100/markets', testData);
        expect(data.length).toEqual(3);
    });

    it('should find runners for a event and market with length of 3', () => {
        const data = CollectionTraversor.findList('events/id:100/markets/id:101/runners', testData);
        expect(data.length).toEqual(5);
    });


});

describe('CollectionTraversor - extra case', () => {
    let testData: any;


    beforeEach(() => {
        testData = TEST_DATA;
    });

    it('find record and then list', () => {
        // find a record
        const eventRecord = CollectionTraversor.findRecord('events/id:119', testData);

        const data = CollectionTraversor.findList('markets', eventRecord);
        expect(data.length).toEqual(3);
    });
});


describe('CollectionTraversor - findList from root', () => {
    let testData: any;


    beforeEach(() => {
        testData = TEST_DATA;
    });

    it('should find markets for an event with a length of 3', () => {
        const data = CollectionTraversor.findList('events/id:100/markets', testData);
        expect(data.length).toEqual(3);
    });

    it('should find runners for a event and market with length of 3', () => {
        const data = CollectionTraversor.findList('events/id:100/markets/id:101/runners', testData);
        expect(data.length).toEqual(5);
    });


});
