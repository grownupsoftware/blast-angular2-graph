import {PathDetails} from '../src/path-details';

describe('PathDetails - no leading slash', () => {

    it('should throw an exception if a leading slash', () => {

        try {
            const pathDetails: PathDetails[] = PathDetails.splitPath('/events');
            fail('should have thrown an exception');
        } catch (blastException) {
            expect(true).toEqual(true);
        }
    });
});

describe('PathDetails - root', () => {
    let pathDetails: PathDetails[];

    beforeEach(() => {
        pathDetails = PathDetails.splitPath('');
    });

    it('should have a length of 1', () => {
        expect(pathDetails.length).toEqual(1);
    });

    it('should have at 1st index that it is root', () => {
        expect(pathDetails[0].isRoot()).toEqual(true);
    });

    it('should have at 1st index that it is a collection (list)', () => {
        expect(pathDetails[0].isCollectionOnly()).toEqual(false);
    });

    it('should have no key field', () => {
        expect(pathDetails[0].getKeyField()).toEqual(undefined);
    });

    it('should have no key value', () => {
        expect(pathDetails[0].getKeyValue()).toEqual(undefined);
    });

});

describe('PathDetails - events', () => {
    let pathDetails: PathDetails[];

    beforeEach(() => {
        pathDetails = PathDetails.splitPath('events');
    });

    it('should have a length of 1', () => {
        expect(pathDetails.length).toEqual(1);
    });

    it('should have at 1st index that it is not root', () => {
        expect(pathDetails[0].isRoot()).toEqual(false);
    });

    it('should have at 1st index that it is a collection (list)', () => {
        expect(pathDetails[0].isCollectionOnly()).toEqual(true);
    });

    it('should have no key field', () => {
        expect(pathDetails[0].getKeyField()).toEqual(undefined);
    });

    it('should have no key value', () => {
        expect(pathDetails[0].getKeyValue()).toEqual(undefined);
    });

});

describe('PathDetails - events/id:101', () => {
    let pathDetails: PathDetails[];

    beforeEach(() => {
        pathDetails = PathDetails.splitPath('events/id:101');
    });

    it('should have a length of 1', () => {
        expect(pathDetails.length).toEqual(1);
    });

    it('should have at 1st index that it is not root', () => {
        expect(pathDetails[0].isRoot()).toEqual(false);
    });

    it('should have at 1st index that it is NOT a collection (list)', () => {
        expect(pathDetails[0].isCollectionOnly()).toEqual(false);
    });

    it('should have a key field', () => {
        expect(pathDetails[0].getKeyField()).toEqual('id');
    });

    it('should have a key value', () => {
        expect(pathDetails[0].getKeyValue()).toEqual('101');
    });

});

describe('PathDetails - events/id:101/markets', () => {
    let pathDetails: PathDetails[];

    beforeEach(() => {
        pathDetails = PathDetails.splitPath('events/id:101/markets');
    });

    it('should have a length of 2', () => {
        expect(pathDetails.length).toEqual(2);
    });

    it('should have at 1st index that it is not root', () => {
        expect(pathDetails[0].isRoot()).toEqual(false);
    });

    it('should have at 1st index that it is NOT a collection (list)', () => {
        expect(pathDetails[0].isCollectionOnly()).toEqual(false);
    });

    it('should have a key field at 1st index', () => {
        expect(pathDetails[0].getKeyField()).toEqual('id');
    });

    it('should have a key value at 1st index', () => {
        expect(pathDetails[0].getKeyValue()).toEqual('101');
    });

    it('should have at 2nd index that it is a collection (list)', () => {
        expect(pathDetails[1].isCollectionOnly()).toEqual(true);
    });

    it('should have a NO key field at 2nd index', () => {
        expect(pathDetails[1].getKeyField()).toEqual(undefined);
    });

    it('should have a NO key value at 2nd index', () => {
        expect(pathDetails[1].getKeyValue()).toEqual(undefined);
    });

});

describe('PathDetails - events/id:101/markets/id:102/runners', () => {
    let pathDetails: PathDetails[];

    beforeEach(() => {
        pathDetails = PathDetails.splitPath('events/id:101/markets/id:102/runners');
    });

    it('should have a length of 3', () => {
        expect(pathDetails.length).toEqual(3);
    });

    it('should have at 1st index that it is not root', () => {
        expect(pathDetails[0].isRoot()).toEqual(false);
    });

    it('should have at 1st index that it is NOT a collection (list)', () => {
        expect(pathDetails[0].isCollectionOnly()).toEqual(false);
    });

    it('should have a key field at 1st index', () => {
        expect(pathDetails[0].getKeyField()).toEqual('id');
    });

    it('should have a key value at 1st index', () => {
        expect(pathDetails[0].getKeyValue()).toEqual('101');
    });

    it('should have at 2nd index that it is NOT a collection (list)', () => {
        expect(pathDetails[1].isCollectionOnly()).toEqual(false);
    });

    it('should have a NO key field at 2nd index', () => {
        expect(pathDetails[1].getKeyField()).toEqual('id');
    });

    it('should have a NO key value at 2nd index', () => {
        expect(pathDetails[1].getKeyValue()).toEqual('102');
    });

    it('should have at 3rd index that it is a collection (list)', () => {
        expect(pathDetails[2].isCollectionOnly()).toEqual(true);
    });

    it('should have a NO key field at 3rd index', () => {
        expect(pathDetails[2].getKeyField()).toEqual(undefined);
    });

    it('should have a NO key value at 3rd index', () => {
        expect(pathDetails[2].getKeyValue()).toEqual(undefined);
    });

});
