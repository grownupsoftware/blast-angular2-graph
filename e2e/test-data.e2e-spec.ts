export const TEST_DATA = {
    'markets': [{
        'eventId': 100,
        'name': 'market 107',
        'id': 107,
        'runners': [{
            'eventId': 100,
            'name': 'runner 110',
            'id': 110,
            'marketId': 107,
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'runner 111',
            'id': 111,
            'marketId': 107,
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'runner 108',
            'id': 108,
            'marketId': 107,
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'runner 112',
            'id': 112,
            'marketId': 107,
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'runner 109',
            'id': 109,
            'marketId': 107,
            'status': 'open'
        }],
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'market 101',
        'id': 101,
        'runners': [{
            'eventId': 100,
            'name': 'new name: 2',
            'id': 103,
            'marketId': 101,
            'status': 'joe'
        }, {
            'eventId': 100,
            'name': 'runner 104',
            'id': 104,
            'marketId': 101,
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'runner 105',
            'id': 105,
            'marketId': 101,
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'runner 102',
            'id': 102,
            'marketId': 101,
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'runner 106',
            'id': 106,
            'marketId': 101,
            'status': 'open'
        }],
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'market 126',
        'id': 126,
        'runners': [{
            'eventId': 119,
            'name': 'runner 129',
            'id': 129,
            'marketId': 126,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 130',
            'id': 130,
            'marketId': 126,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 131',
            'id': 131,
            'marketId': 126,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 127',
            'id': 127,
            'marketId': 126,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 128',
            'id': 128,
            'marketId': 126,
            'status': 'open'
        }],
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'market 132',
        'id': 132,
        'runners': [{
            'eventId': 119,
            'name': 'runner 136',
            'id': 136,
            'marketId': 132,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 133',
            'id': 133,
            'marketId': 132,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 137',
            'id': 137,
            'marketId': 132,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 134',
            'id': 134,
            'marketId': 132,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 135',
            'id': 135,
            'marketId': 132,
            'status': 'open'
        }],
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'market 120',
        'id': 120,
        'runners': [{
            'eventId': 119,
            'name': 'runner 125',
            'id': 125,
            'marketId': 120,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 122',
            'id': 122,
            'marketId': 120,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 123',
            'id': 123,
            'marketId': 120,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 124',
            'id': 124,
            'marketId': 120,
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'runner 121',
            'id': 121,
            'marketId': 120,
            'status': 'open'
        }],
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'market 113',
        'id': 113,
        'runners': [{
            'eventId': 100,
            'name': 'runner 114',
            'id': 114,
            'marketId': 113,
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'runner 118',
            'id': 118,
            'marketId': 113,
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'runner 115',
            'id': 115,
            'marketId': 113,
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'runner 116',
            'id': 116,
            'marketId': 113,
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'runner 117',
            'id': 117,
            'marketId': 113,
            'status': 'open'
        }],
        'status': 'open'
    }],
    'runners': [{
        'eventId': 100,
        'name': 'new name: 2',
        'id': 103,
        'marketId': 101,
        'status': 'joe'
    }, {
        'eventId': 100,
        'name': 'runner 118',
        'id': 118,
        'marketId': 113,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 111',
        'id': 111,
        'marketId': 107,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 104',
        'id': 104,
        'marketId': 101,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 112',
        'id': 112,
        'marketId': 107,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 105',
        'id': 105,
        'marketId': 101,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 133',
        'id': 133,
        'marketId': 132,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 134',
        'id': 134,
        'marketId': 132,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 135',
        'id': 135,
        'marketId': 132,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 127',
        'id': 127,
        'marketId': 126,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 128',
        'id': 128,
        'marketId': 126,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 106',
        'id': 106,
        'marketId': 101,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 121',
        'id': 121,
        'marketId': 120,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 114',
        'id': 114,
        'marketId': 113,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 129',
        'id': 129,
        'marketId': 126,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 122',
        'id': 122,
        'marketId': 120,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 115',
        'id': 115,
        'marketId': 113,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 108',
        'id': 108,
        'marketId': 107,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 102',
        'id': 102,
        'marketId': 101,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 136',
        'id': 136,
        'marketId': 132,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 137',
        'id': 137,
        'marketId': 132,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 130',
        'id': 130,
        'marketId': 126,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 131',
        'id': 131,
        'marketId': 126,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 123',
        'id': 123,
        'marketId': 120,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 116',
        'id': 116,
        'marketId': 113,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 109',
        'id': 109,
        'marketId': 107,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 124',
        'id': 124,
        'marketId': 120,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 117',
        'id': 117,
        'marketId': 113,
        'status': 'open'
    }, {
        'eventId': 100,
        'name': 'runner 110',
        'id': 110,
        'marketId': 107,
        'status': 'open'
    }, {
        'eventId': 119,
        'name': 'runner 125',
        'id': 125,
        'marketId': 120,
        'status': 'open'
    }],
    'events': [{
        'markets': [{
            'eventId': 100,
            'name': 'market 107',
            'id': 107,
            'runners': [{
                'eventId': 100,
                'name': 'runner 110',
                'id': 110,
                'marketId': 107,
                'status': 'open'
            }, {
                'eventId': 100,
                'name': 'runner 111',
                'id': 111,
                'marketId': 107,
                'status': 'open'
            }, {
                'eventId': 100,
                'name': 'runner 108',
                'id': 108,
                'marketId': 107,
                'status': 'open'
            }, {
                'eventId': 100,
                'name': 'runner 112',
                'id': 112,
                'marketId': 107,
                'status': 'open'
            }, {
                'eventId': 100,
                'name': 'runner 109',
                'id': 109,
                'marketId': 107,
                'status': 'open'
            }],
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'market 101',
            'id': 101,
            'runners': [{
                'eventId': 100,
                'name': 'new name: 2',
                'id': 103,
                'marketId': 101,
                'status': 'joe'
            }, {
                'eventId': 100,
                'name': 'runner 104',
                'id': 104,
                'marketId': 101,
                'status': 'open'
            }, {
                'eventId': 100,
                'name': 'runner 105',
                'id': 105,
                'marketId': 101,
                'status': 'open'
            }, {
                'eventId': 100,
                'name': 'runner 102',
                'id': 102,
                'marketId': 101,
                'status': 'open'
            }, {
                'eventId': 100,
                'name': 'runner 106',
                'id': 106,
                'marketId': 101,
                'status': 'open'
            }],
            'status': 'open'
        }, {
            'eventId': 100,
            'name': 'market 113',
            'id': 113,
            'runners': [{
                'eventId': 100,
                'name': 'runner 114',
                'id': 114,
                'marketId': 113,
                'status': 'open'
            }, {
                'eventId': 100,
                'name': 'runner 118',
                'id': 118,
                'marketId': 113,
                'status': 'open'
            }, {
                'eventId': 100,
                'name': 'runner 115',
                'id': 115,
                'marketId': 113,
                'status': 'open'
            }, {
                'eventId': 100,
                'name': 'runner 116',
                'id': 116,
                'marketId': 113,
                'status': 'open'
            }, {
                'eventId': 100,
                'name': 'runner 117',
                'id': 117,
                'marketId': 113,
                'status': 'open'
            }],
            'status': 'open'
        }],
        'name': 'event 100',
        'id': 100,
        'status': 'open'
    }, {
        'markets': [{
            'eventId': 119,
            'name': 'market 126',
            'id': 126,
            'runners': [{
                'eventId': 119,
                'name': 'runner 129',
                'id': 129,
                'marketId': 126,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 130',
                'id': 130,
                'marketId': 126,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 131',
                'id': 131,
                'marketId': 126,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 127',
                'id': 127,
                'marketId': 126,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 128',
                'id': 128,
                'marketId': 126,
                'status': 'open'
            }],
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'market 132',
            'id': 132,
            'runners': [{
                'eventId': 119,
                'name': 'runner 136',
                'id': 136,
                'marketId': 132,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 133',
                'id': 133,
                'marketId': 132,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 137',
                'id': 137,
                'marketId': 132,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 134',
                'id': 134,
                'marketId': 132,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 135',
                'id': 135,
                'marketId': 132,
                'status': 'open'
            }],
            'status': 'open'
        }, {
            'eventId': 119,
            'name': 'market 120',
            'id': 120,
            'runners': [{
                'eventId': 119,
                'name': 'runner 125',
                'id': 125,
                'marketId': 120,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 122',
                'id': 122,
                'marketId': 120,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 123',
                'id': 123,
                'marketId': 120,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 124',
                'id': 124,
                'marketId': 120,
                'status': 'open'
            }, {
                'eventId': 119,
                'name': 'runner 121',
                'id': 121,
                'marketId': 120,
                'status': 'open'
            }],
            'status': 'open'
        }],
        'name': 'event 119',
        'id': 119,
        'status': 'open'
    }]
};

